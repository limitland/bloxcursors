#!/bin/bash

for COLOR in White Black Blue Green Orange Red ; do

	echo "Building ${COLOR}..."
	LCCOLOR=$(echo ${COLOR} | tr '[:upper:]' '[:lower:]')

	mkdir -p tmp
	cp ../build/${COLOR}/*.56.png tmp
	mv tmp/help.frame.1.56.png tmp/help.56.png
        mv tmp/progress.frame.03.56.png tmp/progress.56.png
        mv tmp/wait.frame.03.56.png tmp/wait.56.png
        rm tmp/*frame*

	montage tmp/*56.png tmp/wallpaper.png
	montage -mode concatenate -tile 1x bloxcursors-header.png tmp/wallpaper.png bloxcursors-${LCCOLOR}.png

	rm -rf tmp

done

echo "Building sample..."

	mkdir -p tmp

	cp ../build/White/default.56.png \
	  ../build/White/help.frame.1.56.png \
	  ../build/White/not-allowed.56.png \
	  ../build/White/pirate.56.png \
	  ../build/White/pointer.56.png \
	  ../build/White/progress.frame.03.56.png \
	  ../build/White/text.56.png \
	  ../build/White/wait.frame.03.56.png \
	  ../build/White/X-cursor.56.png \
	  tmp

	montage -geometry 56x56+5+5 tmp/*56.png bloxcursors-sample.png

	rm -rf tmp
