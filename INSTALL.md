# Blox Cursors installation instructions

Basic Installation
------------------

Requirements:
- librsvg <https://wiki.gnome.org/Projects/LibRsvg>
- python-lxml  <https://lxml.de/> <https://pypi.org/project/lxml/>

Checkout the BloxCursors sources with git:

    $ git clone https://gitlab.com/limitland/bloxcursors.git

Or use the source packages from the build pipeline: https://limitland.gitlab.io/bloxcursors/

When you have the required programs, issue:

    $ ./bin/build-cursors

This will read the basic cursors from the SVG files, process them
and compose them into a PNG image.

After all the images have been processed, you should invoke:

    $ make
    $ make install

to create the xcursor files and install them into you personal cursors
folder (‘$HOME/.icons/’).

If you make changes in sources (e.g. edit the svg files) you might have 
to clean the working tree:

    $ make clean

Making changes in the sources is not recommended, but if you need to,
make sure to match colors and transformations as processed by the
'build-cursors' script. Read 'Customizing Cursors' below if you want to
build a custom Cursor theme.

If you want to remove the custom cursor theme from its installed location,
invoke:

    $ make uninstall


Customizing Cursors
-------------------

Apart from the actual SVG files, a lot of customization can be done
while composing the images.

All configuration options are in the ‘.config’ file for each theme.
Each theme has one, and there is a ‘custom.config’ which is a template
for you to copy and customise to build a custom theme.

To start making a custom theme, choose a name and invoke:

    $ export THEMENAME="Purple-and-Green"
    $ make custom-theme

The environment variable ‘THEMENAME’ will be used by each of the
programs to know which theme you have specified. (This is why the
‘export’ command is necessary.) The above step will make the files
‘Purple-and-Green.config’ and ‘Purple-and-Green.theme’ in the
‘Themes’ directory, if they did not already exist. Edit
each of these files to customise your new theme.

The ‘$THEMENAME.config’ file configures the theme-specific behaviour of the
‘bin/render-cursor-image’ program. Run the program with no parameters for a
list of options that can be specified.

The ‘bin/render-cursor-image’ program is used by the ‘bin/build-cursors’
program, so when you want to batch-process the SVG source images you can
either add as many options to the command line as you want, or edit the
default values in the ‘$THEMENAME.config’ file.

Edit the custom files ‘Themes/$THEMENAME.config’ and
‘Themes/$THEMENAME.theme’ files to your liking. Then build and
install your custom cursor theme invoking:

    $ ./bin/build-cursors
    $ make
    $ make install

If you want to remove your custom cursor theme from its installed location,
invoke:

    $ make uninstall


Complete Installation
---------------------

To install all the original BloxCursors issue:

    $ ./install-all

which will process all the configurations in the
BloxCursorsConfigs directory and install all cursor
sets provided into your ~/.icons directory.

If you want to remove all the original BloxCursors from its installed
location, invoke::

    $ ./install-all -u


System Installation
-------------------

For system-wide installation you need to copy the cursor-
directories to you X11 lib directory. Issue these commands:

    $ whereis X11
    $ man 3 xcursor
    $ cd /usr/share/icons
    $ sudo cp -r ~/.icons/BloxCursors* .

Now this will interfere with the cursor theme in your ~/.icons
directory, which will have precedence. You will want to rename
the cursors directory and edit the index.theme file inside.

If you want to make this theme the system-wide default, edit
the file /etc/sysconfig/windowmanager to read:

    X_MOUSE_CURSOR="BloxCursors-White-Regular"

or whatever your (custom) cursors directory is named.

You can also download pre-built RPMs from the build pipeline 
<https://limitland.gitlab.io/bloxcursors/> and install them with:

    $ sudo rpm -Uv BloxCursors-<version>.noarch.rpm

