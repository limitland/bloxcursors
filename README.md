# The BloxCursors README

### Time for a new cursor style

BoxCursors is a X11 mouse theme with a geometric look and feel.
The package includes six colors and five different sizes. 

* Colors: black, blue, red, orange, green and white
* Sizes: extra large, huge, large, regular and small

The BloxCursors are branched from the original ComixCursors by Jens Luetkens, 
Ben Finney and contributors. Please refer to the "AUTHORS" file for details.  

The Blox Cursors are available on gitlab.com:
<https://gitlab.com/limitland/bloxcursors>

The latest build artifacts (releases) are available on gitlab pages:
<https://limitland.gitlab.io/bloxcursors>

INSTALLATION
------------

Refer to the "INSTALL.md" file for requirements and detailed instructions
to build and install.

COPYRIGHT
---------

Refer to the "COPYING" file for copyright statement and grant of
license.

NAMING CURSORS
--------------

The cursors are named according to the freedesktop.org naming convention, 
see the file CURSORNAMES.md for details.
